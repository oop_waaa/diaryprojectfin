/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patcharin.diaryproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author patch
 */
public class WriteActivity {
    public static void main(String[] args) {
         ArrayList<Activity>acti = new ArrayList();
         acti.add(new Activity(1, 1, 2022, "Go to Gym"));
         acti.add(new Activity(2, 1, 2022, "Go to Gym"));
         
         File file = null;
         FileOutputStream fos = null;
         ObjectOutputStream oos = null;
        try {
            file = new File("ativity.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(acti);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteActivity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteActivity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
