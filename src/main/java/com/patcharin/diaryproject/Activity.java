/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.patcharin.diaryproject;

import java.io.Serializable;

/**
 *
 * @author patch
 */
public class Activity implements Serializable{
    private int Date;
    private int Month;
    private int Year;
    private String Note;

    @Override
    public String toString() {
        return "Activity{" + "Date=" + Date + ", Month=" + Month + ", Year=" + Year + ", Note=" + Note + '}';
    }

    public int getDate() {
        return Date;
    }

    public void setDate(int Date) {
        this.Date = Date;
    }

    public int getMonth() {
        return Month;
    }

    public void setMonth(int Month) {
        this.Month = Month;
    }

    public int getYear() {
        return Year;
    }

    public void setYear(int Year) {
        this.Year = Year;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String Note) {
        this.Note = Note;
    }

    public Activity(int Date, int Month, int Year, String Note) {
        this.Date = Date;
        this.Month = Month;
        this.Year = Year;
        this.Note = Note;
    }
}
